package ru.stc.service;

import org.springframework.stereotype.Service;

/**
 * Бизнес-логика
 */

@Service
public class HelloService {

    public String getHello(String name) {
        return "Hello " + name + "!";
    }
}
