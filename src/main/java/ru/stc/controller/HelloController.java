package ru.stc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.stc.entity.UserData;
import ru.stc.service.HelloService;

/**
 * Связывание бизнес-логики и представления
 */
@Controller
public class HelloController {

    private final HelloService helloService;

    @Autowired
    private UserData data;

    @Autowired
    public HelloController(HelloService helloService) {
        this.helloService = helloService;
    }


    @RequestMapping("/hello")
    public String sayHello(Model model) {

        model.addAttribute("message", helloService.getHello(data.getName()));
        return "hello";
    }

    @RequestMapping("/login")
    public String sayHello(Model model, @RequestParam("name") String name) {
        data.setName(name);
        model.addAttribute("message", helloService.getHello(data.getName()));
        return "hello";
    }

}
